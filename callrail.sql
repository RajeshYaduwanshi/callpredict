-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 12, 2020 at 04:49 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `callrail`
--

-- --------------------------------------------------------

--
-- Table structure for table `call_report`
--

CREATE TABLE `call_report` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `phone_number` text NOT NULL,
  `other_phone_number` text NOT NULL,
  `duration` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `audio` text NOT NULL,
  `pick_or_not` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_02_12_065902_create_permission_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `phone` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `state` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `phone`, `created_at`, `updated_at`, `state`) VALUES
(1, 'Mark Begich', '+14157671351', '2020-06-12 09:00:46', '2020-06-12 09:00:46', 'AK'),
(2, 'Lisa Murkowski', '+12174412652', '2020-06-12 09:00:46', '2020-06-12 09:00:46', 'AK'),
(3, 'Jeff Sessions', '+14157671351', '2020-06-12 09:00:46', '2020-06-12 09:00:46', 'AL'),
(4, 'Richard C. Shelby', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'AL'),
(5, 'John Boozman', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'AR'),
(6, 'Mark L. Pryor', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'AR'),
(7, 'Jon Kyl', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'AZ'),
(8, 'John McCain', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'AZ'),
(9, 'Barbara Boxer', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'CA'),
(10, 'Dianne Feinstein', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'CA'),
(11, 'Michael F. Bennet', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'CO'),
(12, 'Mark Udall', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'CO'),
(13, 'Richard Blumenthal', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'CT'),
(14, 'Joseph I. Lieberman', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'CT'),
(15, 'Thomas R. Carper', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'DE'),
(16, 'Christopher A. Coons', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'DE'),
(17, 'Bill Nelson', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'FL'),
(18, 'Marco Rubio', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'FL'),
(19, 'Saxby Chambliss', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'GA'),
(20, 'Johnny Isakson', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'GA'),
(21, 'Daniel K. Akaka', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'HI'),
(22, 'Daniel K. Inouye', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'HI'),
(23, 'Chuck Grassley', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'IA'),
(24, 'Tom Harkin', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'IA'),
(25, 'Mike Crapo', '+14157671351', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'ID'),
(26, 'James E. Risch', '+12174412652', '2020-06-12 09:00:47', '2020-06-12 09:00:47', 'ID'),
(27, 'Richard J. Durbin', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'IL'),
(28, 'Tammy Duckworth', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'IL'),
(29, 'Daniel Coats', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'IN'),
(30, 'Richard G. Lugar', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'IN'),
(31, 'Jerry Moran', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'KS'),
(32, 'Pat Roberts', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'KS'),
(33, 'Mitch McConnell', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'KY'),
(34, 'Rand Paul', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'KY'),
(35, 'Mary L. Landrieu', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'LA'),
(36, 'David Vitter', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'LA'),
(37, 'Scott P. Brown', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MA'),
(38, 'John F. Kerry', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MA'),
(39, 'Benjamin L. Cardin', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MD'),
(40, 'Barbara A. Mikulski', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MD'),
(41, 'Susan M. Collins', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'ME'),
(42, 'Olympia J. Snowe', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'ME'),
(43, 'Carl Levin', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MI'),
(44, 'Debbie Stabenow', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MI'),
(45, 'Al Franken', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MN'),
(46, 'Amy Klobuchar', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MN'),
(47, 'Roy Blunt', '+14157671351', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MO'),
(48, 'Claire McCaskill', '+12174412652', '2020-06-12 09:00:48', '2020-06-12 09:00:48', 'MO'),
(49, 'Roger F. Wicker', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'MS'),
(50, 'Max Baucus', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'MT'),
(51, 'Jon Tester', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'MT'),
(52, 'Richard Burr', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NC'),
(53, 'Kay R. Hagan', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NC'),
(54, 'Kent Conrad', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'ND'),
(55, 'John Hoeven', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'ND'),
(56, 'Mike Johanns', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NE'),
(57, 'Ben Nelson', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NE'),
(58, 'Kelly Ayotte', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NH'),
(59, 'Jeanne Shaheen', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NH'),
(60, 'Frank R. Lautenberg', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NJ'),
(61, 'Robert Menendez', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NJ'),
(62, 'Jeff Bingaman', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NM'),
(63, 'Tom Udall', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NM'),
(64, 'John Ensign', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NV'),
(65, 'Harry Reid', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NV'),
(66, 'Kirsten E. Gillibrand', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NY'),
(67, 'Charles E. Schumer', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'NY'),
(68, 'Sherrod Brown', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'OH'),
(69, 'Rob Portman', '+12174412652', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'OH'),
(70, 'Tom Coburn', '+14157671351', '2020-06-12 09:00:49', '2020-06-12 09:00:49', 'OK'),
(71, 'James M. Inhofe', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'OK'),
(72, 'Jeff Merkley', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'OR'),
(73, 'Ron Wyden', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'OR'),
(74, 'Robert P., Jr. Casey', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'PA'),
(75, 'Patrick J. Toomey', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'PA'),
(76, 'Jack Reed', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'RI'),
(77, 'Sheldon Whitehouse', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'RI'),
(78, 'Jim DeMint', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'SC'),
(79, 'Lindsey Graham', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'SC'),
(80, 'Tim Johnson', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'SD'),
(81, 'John Thune', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'SD'),
(82, 'Lamar Alexander', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'TN'),
(83, 'Bob Corker', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'TN'),
(84, 'John Cornyn', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'TX'),
(85, 'Kay Bailey Hutchison', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'TX'),
(86, 'Orrin G. Hatch', '+14157671351', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'UT'),
(87, 'Mike Lee', '+12174412652', '2020-06-12 09:00:50', '2020-06-12 09:00:50', 'UT'),
(88, 'Mark R. Warner', '+14157671351', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'VA'),
(89, 'Jim Webb', '+12174412652', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'VA'),
(90, 'Patrick J. Leahy', '+14157671351', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'VT'),
(91, 'Bernard Sanders', '+12174412652', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'VT'),
(92, 'Maria Cantwell', '+14157671351', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WA'),
(93, 'Patty Murray', '+12174412652', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WA'),
(94, 'Ron Johnson', '+14157671351', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WI'),
(95, 'Herb Kohl', '+12174412652', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WI'),
(96, 'Joe, III Manchin', '+14157671351', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WV'),
(97, 'John D., IV Rockefeller', '+12174412652', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WV'),
(98, 'John Barrasso', '+14157671351', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WY'),
(99, 'Michael B. Enzi', '+12174412652', '2020-06-12 09:00:51', '2020-06-12 09:00:51', 'WY');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_number`
--

CREATE TABLE `user_number` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `where_used` text NOT NULL,
  `where_display` text NOT NULL,
  `account_number_name` text NOT NULL,
  `phone_number` text NOT NULL,
  `tracking_option` text NOT NULL,
  `whisper_message` text NOT NULL,
  `call_recording_on` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_number`
--
ALTER TABLE `user_number`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_number`
--
ALTER TABLE `user_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
